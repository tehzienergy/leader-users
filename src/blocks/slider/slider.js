//$('.slider__content').slick({
//  arrows: false,
//  vertical: true,
//  infinite: true,
//  fade: true,
////  autoplay: true,
//  waitForAnimate: true,
//  verticalSwiping: true,
//});
//
//function prevslide() {
//  $('.slick-active').prev('.slick-slide').addClass('slick-slide--prev');
//}
//
//prevslide();
//
//$('.slider__content').on('beforeChange', function(event, slick, currentSlide, nextSlide){
//  $('.slick-slide').removeClass('slick-slide--prev');
//  prevslide();
//});

function change() {
  bar.set(0);
  $('.slider__item--active').addClass('slider__item--fading');
  setTimeout(function() {
    $('.slider__item--active').removeClass('slider__item--active slider__item--fading').addClass('slider__item--old').next('.slider__item').addClass('slider__item--active');
  }, 1000);
  $('.slider__item--old').detach().removeClass('slider__item--old').appendTo('.slider__content');
  bar.animate(1.0); 
}


window.setInterval(change, 5000);

var bar = new ProgressBar.Circle(progress, {
  strokeWidth: 20,
  easing: 'easeInOut',
  duration: 5000,
  color: 'rgba(255, 255, 255, .7)',
  trailColor: '#eee',
  trailWidth: .1,
  svgStyle: null
});

bar.animate(1.0);  // Number from 0.0 to 1.0